package com.notification;

import javax.management.Notification;
import javax.management.NotificationFilter;

public class Tick1Filter implements NotificationFilter
{

    @Override
    public boolean isNotificationEnabled(Notification notification)
    {
        // Filter notifications of type 'TICK1' from all notifications
        return (notification.getType().equals("TICK1")) ? true : false;
    }
}
