package com.notification;

import java.util.Scanner;


public class EntryPoint
{
	public static void main(String[] args)
	{
		NotificationHubExample me = new NotificationHubExample();

		try
		{
			Scanner theInput = new Scanner(System.in);

			boolean isInteractive = true;
			while (isInteractive)
			{
				System.out.println("> ");
				String theEntry = theInput.nextLine();
				String[] theCommand = theEntry.toString().trim().split(" ");

				if (theCommand == null || theCommand.length == 0 || theCommand[0].isEmpty())
				{
					continue;
				}
				// Exit console
				if (theCommand[0].toLowerCase().equals("exit"))
				{
					System.exit(0);
					continue;
				}
				// Start listening to broadcasted notifications
				if (theCommand[0].toLowerCase().equals("listen"))
				{
					me.listenNotifications();
					continue;
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
