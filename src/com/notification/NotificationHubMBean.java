package com.notification;

public interface NotificationHubMBean
{
    int getNotificationCount();
}
