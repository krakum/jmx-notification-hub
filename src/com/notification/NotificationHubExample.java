package com.notification;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.util.Date;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.Notification;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.timer.Timer;

public class NotificationHubExample implements NotificationListener
{
	private ObjectName myObjectName = null;
	private MBeanServer myMBeanServer = ManagementFactory.getPlatformMBeanServer();
	private NotificationFilter myTick1Filter = null;

	/**
	 * Constructor
	 */
	public NotificationHubExample()
	{
	}

	public void listenNotifications()
	{
		try
		{
			myObjectName = new ObjectName("NotificationHub:type=NotificationHubMBean");
			NotificationHubMBean nh = new NotificationHub();

			myTick1Filter = new Tick1Filter();
			myMBeanServer.registerMBean(nh, myObjectName);

			// Add HubMBean as a listener to the hub
			// This listener will handle all notifications emitted by any registered MBeans
			myMBeanServer.addNotificationListener(myObjectName, this, myTick1Filter, null);

			// Create and register a few timer MBeans to MBeanServer
			timerAMBeans(myMBeanServer);
			timerBMBeans(myMBeanServer);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * Create a few timer MBeans of type TimerA that broadcast regular notifications
	 * TimerA - type of MBean
	 * @param mbs
	 *        MBeanServer instance
	 */
	private void timerAMBeans(MBeanServer mbs)
	{
		try
		{
			for (int i = 1 ; i < 3; i++)
			{
				myObjectName = new ObjectName("Timers:type=TimerA,name=TimerMBean "+i);
				Timer timer = new Timer();
				timer.addNotification("TICK"+i, "tick from TimerBean "+i,
						null,
						new Date(),
						1000 * i);
				mbs.registerMBean(timer, myObjectName);
				timer.start();
			}
		}
		catch (MalformedObjectNameException e)
		{
			e.printStackTrace();
		}
		catch (InstanceAlreadyExistsException e)
		{
			e.printStackTrace();
		}
		catch (MBeanRegistrationException e)
		{
			e.printStackTrace();
		}
		catch (NotCompliantMBeanException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Create a few timer MBeans of type TimerB that broadcast regular notifications
	 * TimerB - type of MBean
	 * @param mbs
	 *        MBeanServer instance
	 */
	private void timerBMBeans(MBeanServer mbs)
	{
		try
		{
			for (int i = 3 ; i < 5; i++)
			{
				myObjectName = new ObjectName("Timers:type=TimerB,name=TimerMBean "+i);
				Timer timer = new Timer();
				timer.addNotification("TOCK"+i, "tock from TimerBean "+i,
						null,
						new Date(),
						1000 * i);
				mbs.registerMBean(timer, myObjectName);
				timer.start();
			}
		}
		catch (MalformedObjectNameException e)
		{
			e.printStackTrace();
		}
		catch (InstanceAlreadyExistsException e)
		{
			e.printStackTrace();
		}
		catch (MBeanRegistrationException e)
		{
			e.printStackTrace();
		}
		catch (NotCompliantMBeanException e)
		{
			e.printStackTrace();
		}
	}


	@Override
	public void handleNotification(Notification notification, Object handback)
	{
		if (notification != null)
		{
			formattedNotification(notification);
		}
		else
		{
			System.out.println("Failed to write notifications to log.");
		}
		//        System.out.println(formattedNotification(notification));
		//      System.out.println("Got notification : "+notification);
		//      System.out.println("Type: " + notification.getType());
		//
		//      String type = notification.getType();
		//
		//      if (type.equals("TimerA"))
		//      {
		//          try
		//          {
		//              System.out.println(notification.getMessage());
		//
		//              myMBeanServer.unregisterMBean((ObjectName)handback);
		//              System.out.println(handback + " unregistered.");
		//          }
		//          catch (JMException e)
		//          {
		//              e.printStackTrace();
		//          }
		//      }
	}

	public void formattedNotification(Notification notification)
	{
		StringBuilder theBuilder = new StringBuilder();
		theBuilder.append(notification.getSequenceNumber()).append(" ");
		theBuilder.append(notification.getTimeStamp()).append(" ");
		theBuilder.append(notification.getType()).append(" ");
		theBuilder.append(notification.getMessage());
		String formattedNotificationMessage = theBuilder.toString();
		InputStream in = new ByteArrayInputStream(formattedNotificationMessage.getBytes());

		File theDirectory = new File("logs");
		File theNotificationLog = new File("logs/notifications.log");
		BufferedWriter bufferedWriter = null;

		try
		{
			if (!(theDirectory.exists() || theNotificationLog.exists()))
			{
				theDirectory.mkdir();
				theNotificationLog.createNewFile();
			}
			bufferedWriter = new BufferedWriter(new FileWriter(theNotificationLog.getAbsoluteFile(), true));
			String theEOL = System.getProperty("line.separator");

			bufferedWriter.write(formattedNotificationMessage);
			bufferedWriter.write(theEOL);
			bufferedWriter.flush();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (bufferedWriter != null)
			{
				try
				{
					bufferedWriter.close();
				}
				catch (IOException ex) 
				{
					ex.printStackTrace();
				}
			}
		}
	}

}
